from subprocess import run
run("rustc -g ./sqrt-decomposition.rs", shell=True, check=True);
while True:
    run("python3 generator.py > in.txt", shell=True, check=True)
    run("python3 bruteforce.py < in.txt > out1.txt", shell=True, check=True)
    run("./sqrt-decomposition < in.txt > out2.txt", shell=True, check=True)
    run("diff -q out1.txt out2.txt", shell=True, check=True)
