#[allow(unused_imports)]
use std::{assert, char::*, cmp::*, collections::*, io::*, mem::*, *};
#[allow(unused)]
macro_rules! input { ($($r:tt)*) => { let stdin = std::io::stdin(); let mut bytes = std::io::Read::bytes(std::io::BufReader::new(stdin.lock())); let mut next = move || -> String{ bytes.by_ref().map(|r|r.unwrap() as char).skip_while(|c|c.is_whitespace()).take_while(|c|!c.is_whitespace()).collect() }; input_inner!{next, $($r)*} }; }
#[allow(unused)]
macro_rules! input_inner { ($next:expr) => {}; ($next:expr, ) => {}; ($next:expr, $var:ident : $t:tt $($r:tt)*) => { let $var = read_value!($next, $t); input_inner!{$next $($r)*} }; }
#[allow(unused)]
macro_rules! read_value { ($next:expr, ( $($t:tt),* )) => { ( $(read_value!($next, $t)),* ) }; ($next:expr, [ $t:tt ; $len:expr ]) => { (0..$len).map(|_| read_value!($next, $t)).collect::<Vec<_>>() }; ($next:expr, chars) => { read_value!($next, String).chars().collect::<Vec<char>>() }; ($next:expr, usize1) => { read_value!($next, usize) - 1}; ($next:expr, [ $t:tt ]) => {{ let len = read_value!($next, usize);(0..len).map(|_| read_value!($next, $t)).collect::<Vec<_>>()}};($next:expr, $t:ty) => {$next().parse::<$t>().expect("Parse error")};}
#[allow(unused)]
macro_rules! debug { ($($format:tt)*) => (write!(std::io::stderr(), $($format)*).unwrap()); }
#[allow(unused)]
macro_rules! debugln { ($($format:tt)*) => (writeln!(std::io::stderr(), $($format)*).unwrap()); }

fn solve() {
    let out = std::io::stdout();
    let mut out = BufWriter::new(out.lock());
    #[allow(unused)]
    macro_rules! puts { ($($format:tt)*) => (let _ = write!(out,$($format)*);); }
    struct SQRT {
        n: usize,
        len: usize,
        a: Vec<i64>,
        complete: Vec<bool>,
        sum: Vec<i64>,
        upd: Vec<i64>,
    }
    impl SQRT {
        fn new(n: usize) -> Self {
            let len = (n as f64).sqrt().ceil() as usize;
            SQRT {
                n,
                len,
                a: vec![0; n],
                upd: vec![0; len],
                sum: vec![0; len],
                complete: vec![false; len],
            }
        }

        fn drop_sqrt(&mut self, block_id: usize) {
            for i in block_id * self.len..(block_id + 1) * self.len {
                self.a[i] = self.upd[block_id];
            }

            self.complete[block_id] = false;
        }

        fn assign_slow(&mut self, l: usize, r: usize, x: i64) {
            let block_id = l / self.len;

            if self.complete[block_id] {
                self.drop_sqrt(block_id);
            }

            assert!(l < self.n);
            assert!(r < self.n);

            for i in l..=r {
                let diff = x - self.a[i];
                self.sum[block_id] += diff;
                self.a[i] = x;
            }
        }

        fn assign_sqrt(&mut self, l_block: usize, r_block: usize, x: i64) {
            for i in l_block..=r_block {
                self.upd[i] = x;
                self.complete[i] = true;
                self.sum[i] = x * self.len as i64;
            }
        }

        fn sum_slow(&self, l: usize, r: usize) -> i64 {
            if self.complete[l / self.len] {
                (r - l + 1) as i64 * self.upd[l / self.len]
            } else {
                self.a[l..r + 1].iter().sum()
            }
        }

        fn sum_sqrt(&self, l_block: usize, r_block: usize) -> i64 {
            self.sum[l_block..r_block + 1].iter().sum()
        }

        fn assign(&mut self, l: usize, r: usize, x: i64) {
            let cl = l / self.len;
            let cr = r / self.len;

            if cl == cr {
                self.assign_slow(l, r, x);
            } else {
                let l1 = l;
                let r1 = (cl + 1) * self.len - 1;

                let l2 = cl + 1;
                let r2 = cr - 1;

                let l3 = cr * self.len;
                let r3 = r;

                assert!(r1 < self.n);
                assert!(l2 < self.len);
                assert!(r2 < self.len);
                assert!(l3 < self.n);

                self.assign_slow(l1, r1, x);
                self.assign_sqrt(l2, r2, x);
                self.assign_slow(l3, r3, x);
            }
        }

        fn sum(&self, l: usize, r: usize) -> i64 {
            assert!(l < self.n);
            assert!(r < self.n);

            let cl = l / self.len;
            let cr = r / self.len;

            if cl == cr {
                self.sum_slow(l, r)
            } else {
                let l1 = l;
                let r1 = (cl + 1) * self.len - 1;

                let l2 = cl + 1;
                let r2 = cr - 1;

                let l3 = cr * self.len;
                let r3 = r;

                assert!(r1 < self.n);
                assert!(l2 < self.len);
                assert!(r2 < self.len);
                assert!(l3 < self.n);

                self.sum_slow(l1, r1) + self.sum_sqrt(l2, r2) + self.sum_slow(l3, r3)
            }
        }
    }
    let mut sqrt = SQRT::new(get());
    for _ in 0..get() {
        let kind: usize = get();
        let l: usize = get();
        let r: usize = get();
        // indexing is self.not apparent from the description:
        let l = l - 1;
        let r = r - 1;
        match kind {
            1 => {
                let x: i64 = get();
                sqrt.assign(l, r, x);
            }
            2 => {
                puts!("{}\n", sqrt.sum(l, r));
            }
            _ => panic!("a million miles out of reach"),
        }
    }
}
fn main() {
    // In order to avoid potential stack overflow, spawn a self.new thread.
    let stack_size = 104_857_600; // 100 MB
    let thd = std::thread::Builder::new().stack_size(stack_size);
    thd.spawn(|| solve()).unwrap().join().unwrap();
}

#[allow(dead_code)]
fn get_word() -> String {
    let stdin = std::io::stdin();
    let mut stdin = stdin.lock();
    let mut u8b: [u8; 1] = [0];
    loop {
        let mut buf: Vec<u8> = Vec::with_capacity(16);
        loop {
            let res = stdin.read(&mut u8b);
            if res.unwrap_or(0) == 0 || u8b[0] <= b' ' {
                break;
            } else {
                buf.push(u8b[0]);
            }
        }
        if buf.len() >= 1 {
            let ret = String::from_utf8(buf).unwrap();
            return ret;
        }
    }
}
#[allow(dead_code)]
fn get<T: std::str::FromStr>() -> T {
    get_word().parse().ok().unwrap()
}

// https://qiita.com/tanakh/items/0ba42c7ca36cd29d0ac8
