N, M = map(int, input().split())
a = [0 for _ in range(N)]
for i in range(M):
    nums = list(map(int, input().split()))
    #print(nums[0])
    if nums[0] == 1:
        l, r, x = nums[1:]
        for i in range(l - 1, r):
            a[i] = x
    if nums[0] == 2:
        l, r = nums[1:]
        print(sum(a[l-1:r]))
